cimport clibusbtc08

from libc.stdint cimport int8_t, int16_t, int32_t

cdef class TC08:

    # --------- CLASS ATTRIBUTES AND METHODS -----------
    
    # Classs attributes :
    # dictionay of all handles, with S/N as key and handle as value 
    __handles = {}

    # Dictionary of error codes
    __error_code = {
        0 : "NO ERROR",
        1 : "OS NOT SUPPORTED",
        2 : "NO CHANNELS SET",
        3 : "INVALID PARAMETER",
        4 : "HARDWARE VERSION NOT SUPPORTED",
        5 : "INCORRECT MODE",
        6 : "OPEN UNIT ASYNC CALLED TWICE",
        7 : "CANNOT GET REPLY FROM DEVICE",
        8 : "FIRMWARE DOWNLOAD FAIL",
        9 : "MISSING/CORRUPTED EEPROM",
        10 : "DEVICE NOT FOUND",
        11 : "THREADING FUNCTION FAIL",
        12 : "CANNOT GET USB PIPE INFO",
        13 : "CALIBRATION DATE NOT FOUND",
        14 : "OLD DRIVER FOUND ON SYSTEM",
        15 : "PC LOST COMMUNICATION WITH DEVICE"
        }

    # Dictionary of TC08 Units
    __units_code = {
        "CENTIGRADE" : 0,
        "FAHRENHEIT" : 1,
        "KELVIN" : 2,
        "RANKINE" : 3
        }    
    
    # Class internal attributes -- accessors defined below
    # --- unique handle to unit
    cdef int16_t __handle    

    # -- Properties read from unit
    cdef object __driver_version
    cdef int16_t __kernel_driver_version
    cdef int16_t __hardware_version
    cdef int16_t __model_number
    cdef object __serial_number
    cdef object __calibration_date

    # -- mains frequency
    cdef int16_t __mains_frequency

    # -- temperature units
    cdef int16_t __units

    # -- temperature channels
    cdef float __temp_1
    cdef float __temp_2
    cdef float __temp_3
    cdef float __temp_4
    cdef float __temp_5
    cdef float __temp_6
    cdef float __temp_7
    cdef float __temp_8
    cdef float __temp_cj

    # -- overflow state for each channel
    cdef bint __ovf_1
    cdef bint __ovf_2
    cdef bint __ovf_3
    cdef bint __ovf_4
    cdef bint __ovf_5
    cdef bint __ovf_6
    cdef bint __ovf_7
    cdef bint __ovf_8
    cdef bint __ovf_cj
        
    # Hidden class hidden method to open all units    
    @staticmethod
    def __open_units() :
        """
        Private class method to open all TC08 units
        Once open, it fills a dictionary to associate each unit's
        serial number with the assigned handle.
        """
        
        cdef int16_t handle = 9999
        cdef int16_t retVal
        cdef int16_t error_code
        cdef int8_t serial_number[clibusbtc08.USBTC08_MAX_SERIAL_CHARS]
        
        # Open units until all opened or error
        while handle > 0 :
            handle = clibusbtc08.usb_tc08_open_unit()
            #print "__open_units:","retval:",handle
                            
            if handle > 0 :
                # Opened a unit - get its serial number
                retVal = clibusbtc08.usb_tc08_get_unit_info2(handle,
                                                             serial_number, clibusbtc08.USBTC08_MAX_SERIAL_CHARS,
                                                             clibusbtc08.USBTC08LINE_BATCH_AND_SERIAL)
                #print "SERIAL NUMBER:",serial_number
                if retVal == 0 :
                    print "ERROR : FAILED TO GET SERIAL NUMBER DURING INITIALISATION"
                    error_code = clibusbtc08.usb_tc08_get_last_error(handle)
                    print TC08.__error_code[error_code]
                    return
                
                # Fill handles dictionary with S/N as key and handle as value
                TC08.__handles[serial_number] = handle

        # While loop terminated ... check it finised cleanly        
        if handle == -1 :
            print "ERROR : FAILED TO OPEN UNIT"
            error_code = clibusbtc08.usb_tc08_get_last_error(0)
            print TC08.__error_code[error_code]
            return

        #print "__open_units:","Found ",len(TC08.__handles),"unit(s)s"
        #print "__open_units:",TC08.__handles

    # Public Static method to close all units
    @staticmethod
    def close_units() :
        """
        Class methods to close all open units
        """
        
        cdef int16_t unit
        cdef int16_t retVal
    
        while TC08.__handles :
            sn,unit = TC08.__handles.popitem()            
            retVal = clibusbtc08.usb_tc08_close_unit(unit)            
            if retVal == 0 :
                print "ERROR: FAILED TO CLOSE UNIT",unit
            else :
                pass
                #print "closed unit",sn
                
        #print "All units closed"


    # --------- CLASS INSTANCE METHODS AND ATTRIBUTES --------
        
    # Constructor
    def __init__(self, serial_number) :
        self.__serial_number = serial_number
        
    # Open unit
    def open_unit(self) :
        """
        Method to open communications to TC08 unit. This must be
        called before any other method
        """
        
        # if handles is empty, then open all units
        if len(TC08.__handles) == 0 :
            #print "First TC08 instance.  Open all units"
            TC08.__open_units()

        # Get the handle for the unit
        try :
            self.__handle = TC08.__handles[self.serial_number]
            self.__get_unit_info()
        except KeyError :
            self.__handle = 0  # Set handle to 0 to indicate invalid handle
            print self.serial_number, " not found"
            
        #print self.handle, self.serial_number

    def close_unit(self) :
        """
        When done with the TC08, call this method to close
        communication to the unit
        """

        cdef int16_t retVal
        
        if self.handle == 0 :
            print "ERROR: No valid handle"
            return
        
        retVal = clibusbtc08.usb_tc08_close_unit(self.handle)            
        if retVal == 0 :
            print "ERROR: FAILED TO CLOSE UNIT",self.serial_number
            self.get_last_error()
        else :
            #print "closed unit"
            TC08.__handles.pop(self.serial_number)
        

    def get_last_error(self) :
        """
        Get the last error of the unit
        """
        cdef int16_t retVal
        retVal = clibusbtc08.usb_tc08_get_last_error(self.handle)

        if retVal == -1 :
            print "ERROR : INVALID HANDLE"
            return

        print "ERROR CODE: ",retVal," --",TC08.__error_code[retVal]        

        
    def __get_unit_info(self) :
        """
        Get unit info:
        - Driver Version 
        - Kernel Driver Version
        - Hardware Version
        - Model Number
        - Calibration Date
        """

        cdef int16_t retVal
        cdef clibusbtc08.USBTC08_INFO info
        
        info.size = sizeof(clibusbtc08.USBTC08_INFO)
        retVal = clibusbtc08.usb_tc08_get_unit_info(self.handle, &info)  

        if retVal == 0 :
            print "ERROR : FAILED TO GET UNIT INFO"
            self.get_last_error()
            return

        self.__driver_version = info.DriverVersion
        self.__kernel_driver_version = info.PicoppVersion
        self.__hardware_version = info.HardwareVersion
        self.__model_number = info.Variant
        self.__calibration_date = info.szCalDate

    
    # ------ GETTER/SETTERS FOR TC08 -------------
    # Read-only attributes

    @property
    def serial_number(self) :
        return self.__serial_number

    @property
    def handle(self) :
        return self.__handle

    @property
    def driver_version(self) :
        return self.__driver_version

    @property
    def kernel_driver_version(self) :
        return self.__kernel_driver_version

    @property
    def hardware_version(self) :
        return self.__hardware_version

    @property
    def model_number(self) :
        return self.__model_number

    @property
    def calibration_date(self) :
        return self.__calibration_date


    @property 
    def minimum_interval(self) :
        """
        Return the minimum sampling interval of current configuration
        in ms
        """
                
        cdef int32_t retVal
        retVal = clibusbtc08.usb_tc08_get_minimum_interval_ms(self.handle)

        if retVal == 0 :
            print "ERROR : FAILED TO GET MINIMUM INTERVAL"
            self.get_last_error()

        return retVal



    
    # Read/Write attributes

    ##### METHOD TO SET/RECALL UNIT #######
    @property
    def units(self) :
        """
        Return the unit code
        """
        return self.__units

    @units.setter
    def units(self,temperature_unit) :
        """
        Take the temperauture units (as string) and internally convert it to the units code
        """

        #print "UNITS FUNC:",temperature_unit.upper()
        #print "UNITS FUNC:",TC08.__units_code[temperature_unit.upper()]
        
        try :            
            self.__units = TC08.__units_code[temperature_unit.upper()]
        except KeyError :
            print "ERROR: Temperature unit",temperature_unit,"is not supported"
    
    @property
    def mains_frequency(self) :
        """
        Mains frequency rejected
        """
        return self.__mains_frequency

    @mains_frequency.setter    
    def mains_frequency(self,frequency) :
        """
        Instructs unit to reject 50Hz or 60Hz
        """
        cdef int16_t retVal

        # Retun error if mains_frequency is not 50Hz nor 60Hz
        if (frequency != 50) and (frequency != 60) :
            print "ERROR: Can only reject 50Hz or 60Hz, not",frequency,"Hz"
            self.__mains_frequency = 0
            return

        #...at this stage, mains_frequency is either 50Hz or 60Hz
        self.__mains_frequency = frequency
        sixty_hertz = 1 if frequency == 60 else 0
        retVal = clibusbtc08.usb_tc08_set_mains(self.handle, sixty_hertz)
            
        if retVal == 0 :
            print "ERROR : FAILED TO SET MAINS FREQUENCY"
            self.get_last_error()
            
        
    # ------- THERMOMETRY CHANNELS METHODS--------

    def set_channel(self,channel, tc_type) :
        """
        Set channel with thermocouple type
        thermocouple options are:
        B,E,J,K,N,R,S,T,X,DISABLE
        - B-T : B-T type thermcouple
        - X : Voltage reading
        - DISABLE : Disable channel
        """

        # Convert "DISABLE" string to " ", which is TC08 code to disable a channel
        if tc_type == "DISABLE" :
            tc_type = " "
        
        cdef int16_t retVal
        retVal = clibusbtc08.usb_tc08_set_channel(self.handle,channel,
                                                      ord(tc_type.upper()))
        
        if retVal == 0 :
            print "ERROR SETTING CHANNEL"
            self.get_last_error()
            
    def read_channels(self) :
        """
        Read out temperature of all channels
        """
        
        cdef int16_t retVal
        cdef float temp[9]
        cdef int16_t overflow

        # Read out temperatures
        retVal = clibusbtc08.usb_tc08_get_single(self.handle, temp,
                                                     &overflow,self.units) 

        if retVal == 0 :
            print "ERROR READING CHANNELS"
            self.get_last_error()
            return

        # Copy temperature data
        self.__temp_1 = temp[0]
        self.__temp_2 = temp[1]
        self.__temp_3 = temp[2]
        self.__temp_4 = temp[3]
        self.__temp_5 = temp[4]
        self.__temp_6 = temp[5]
        self.__temp_7 = temp[6]
        self.__temp_8 = temp[7]
        self.__temp_cj = temp[8]

        # Copy overflow bits
        # -- convert overflow int to array of bools
        overflow_bool = [int(i) for i in "{0:09b}".format(overflow)]
        self.__ovf_1 = overflow_bool[0]
        self.__ovf_2 = overflow_bool[1]
        self.__ovf_3 = overflow_bool[2]
        self.__ovf_4 = overflow_bool[3]
        self.__ovf_5 = overflow_bool[4]
        self.__ovf_6 = overflow_bool[5]
        self.__ovf_7 = overflow_bool[6]
        self.__ovf_8 = overflow_bool[7]
        self.__ovf_cj = overflow_bool[8]

            
    # Acccessors to channel temperature
    @property
    def temp_1(self) :
        return self.__temp_1

    @property
    def temp_2(self) :
        return self.__temp_2
    
    @property
    def temp_3(self) :
        return self.__temp_3

    @property
    def temp_4(self) :
        return self.__temp_4

    @property
    def temp_5(self) :
        return self.__temp_5

    @property
    def temp_6(self) :
        return self.__temp_6

    @property
    def temp_7(self) :
        return self.__temp_7

    @property
    def temp_8(self) :
        return self.__temp_8

    @property
    def temp_cj(self) :
        return self.__temp_cj

    # Accessors to channel temperature overflow 
    @property
    def ovf_1(self) :
        return self.__ovf_1

    @property
    def ovf_2(self) :
        return self.__ovf_2
    
    @property
    def ovf_3(self) :
        return self.__ovf_3

    @property
    def ovf_4(self) :
        return self.__ovf_4

    @property
    def ovf_5(self) :
        return self.__ovf_5

    @property
    def ovf_6(self) :
        return self.__ovf_6

    @property
    def ovf_7(self) :
        return self.__ovf_7

    @property
    def ovf_8(self) :
        return self.__ovf_8

    @property
    def ovf_cj(self) :
        return self.__ovf_cj

    
