# Cython pxd file for libusbtc08 header

from libc.stdint cimport int8_t, int16_t, int32_t

cdef extern from "usbtc08.h" :
    enum: USBTC08_MAX_VERSION_CHARS
    enum: USBTC08_MAX_SERIAL_CHARS
    enum: USBTC08_MAX_DATE_CHARS
    enum: USBTC08LINE_BATCH_AND_SERIAL

    ctypedef struct USBTC08_INFO:
        int16_t size;
        int8_t  DriverVersion[USBTC08_MAX_VERSION_CHARS];
        int16_t PicoppVersion;
        int16_t HardwareVersion;
        int16_t Variant;
        int8_t  szSerial[USBTC08_MAX_SERIAL_CHARS];
        int8_t  szCalDate[USBTC08_MAX_DATE_CHARS];

    
    int16_t usb_tc08_open_unit()	   
    int16_t usb_tc08_close_unit(int16_t handle)
    int16_t usb_tc08_set_mains(int16_t handle, int16_t sixty_hertz)
    int32_t usb_tc08_get_minimum_interval_ms(int16_t handle)
    int16_t usb_tc08_get_unit_info(int16_t handle, USBTC08_INFO* info)
    int16_t usb_tc08_get_unit_info2(int16_t handle, int8_t *string, int16_t string_length, int16_t line)
    int16_t usb_tc08_get_last_error(int16_t handle)
    int16_t usb_tc08_set_channel(int16_t handle, int16_t channel, int8_t tc_type)
    int16_t usb_tc08_get_single(int16_t handle, float* temp, int16_t* overflow_flags,
                                int16_t units)
